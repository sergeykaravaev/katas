/**
 * Returns an array of distinct node names met in the specified tree.
 *
 * @param {Object} rootNode Root node of the tree.
 *
 * @example
 *      getDistinctNames({
 *          name: "abc",
 *          nodes: [
 *              {
 *                  name: "name-1",
 *              }
 *          ]
 *      });
 * @returns {Array<string>} Array of distinct names ordered alphabetically.
 */
function getDistinctNames(rootNode) {
    return [];
}

module.exports = getDistinctNames;
