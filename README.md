# Katas

### Clone
```
git clone https://sergeykaravaev@bitbucket.org/sergeykaravaev/katas.git
```

### Install
```
cd katas
npm install
```

### Run
```
npm run test
```
